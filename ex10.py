tabby_cat = "\tI'm tabbed in."
split_cat = "I'm split \non a line."
backslash_cat = "I'm \\ a \\ cat."

fat_cat = """
I'll do to a list:
\t* Cat food
\t* Fishie food
\t* Catnip\n\t* Grass
"""

print tabby_cat
print split_cat
print backslash_cat
print fat_cat
